import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import Header from './Header';
import Body from './Body';

import{
  AdMobBanner,
  AdMobInterstitial,
  PublisherBanner,
  AdMobRewarded
} from 'expo'

export default class App extends React.Component {
  constructor() {
    super();
    this.state = {
      tareas: [],
      texto: '',
      cargando: true,
    };
  }

  componentDidMount() {
    this.recuperarEnTlf();
  }

  estabelecerTexto=(value) => {
    console.log(value);
    this.setState({ texto: value });
  }

  agregarTarea = () => {
    const nuevasTareas = [...this.state.tareas, { texto: this.state.texto, key: Date.now() }];
    this.guardarEnTlf(nuevasTareas);
    this.setState({
      tareas: nuevasTareas,
      texto: '',

    });
  }

  eliminarTarea= (id) => {
    const nuevasTareas = this.state.tareas.filter(tarea => tarea.key !== id);
    this.guardarEnTlf(nuevasTareas);
    this.setState({
      tareas: nuevasTareas,
    });
  }

  guardarEnTlf=(tareas) => {
    AsyncStorage.setItem('@appTareas:tareas', JSON.stringify(tareas))
      .then((valor) => {
        console.log(valor);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  recuperarEnTlf = () => {
    AsyncStorage.getItem('@appTareas:tareas')
      .then((valor) => {
        console.log(valor);
        console.log(JSON.parse(valor));
        this.setState({
          cargando: false,
        });
        if (valor !== null) {
          const nuevasTareas = JSON.parse(valor);
          this.setState({
            tareas: nuevasTareas,
          });
        }
      })
      .catch((error) => {
        console.log(error);
        this.setState({
          cargando: false,
        });
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          texto={this.state.texto}
          cambiarTexto={this.estabelecerTexto}
          agregar={this.agregarTarea}
        />

        <Body tareas={this.state.tareas} eliminar={this.eliminarTarea} cargando={this.state.cargando} />
        <AdMobBanner
          style={styles.bottomBAnner}
          bannerSize='fullBanner'
          adUnitID='ca-app-pub-9656783999447024~7001195304'
          testDeviceID='EMULATOR'
          didFailToReceiveAdWithError={this.bannerError}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',

  },
  mensaje: {
    backgroundColor: '#808080',
  },
  botomBanner:{
    position:'absolute',
    bottom:0,
  },

});
