import React from 'react';
import { StyleSheet, Text, View, FlatList, ActivityIndicator } from 'react-native';
import Tarea from './Tarea';

class Body extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        {this.props.cargando &&
          <ActivityIndicator
            size="large"
            color="gray"
            />
      }
        { !this.props.cargando &&
          <FlatList
            data={this.props.tareas}
            renderItem={({ item }) => <Tarea item={item} eliminar={this.props.eliminar} />}
              />
       }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 4,
    backgroundColor: '#fffcfb',
    paddingHorizontal: 10,
  },
});

export default Body;
