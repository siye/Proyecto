import React from 'react';
import { StyleSheet, Text, View, TextInput } from 'react-native';


class Header extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <TextInput
          style={styles.texto}
          onChangeText={this.props.cambiarTexto}
          onSubmitEditing={this.props.agregar}
          value={this.props.texto}
          placeholder="Nueva Tarea..."
          placeholderTextColor="gray"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#d0ffd0',
    justifyContent: 'center',
    paddingHorizontal: 26,
  },
  texto: {
    fontSize: 24,
  },
});

export default Header;
